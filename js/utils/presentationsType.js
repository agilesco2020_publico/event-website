const presentationsType = {//tipo de presentaciones
    "TALLER": {
        "name": "TALLER",
        "color": "red",
    },
    "PRESENTACION": {
        "name": "PRESENTACIÓN",
        "color": "orange",
    },
    "VIDEO": {
        "name": "VIDEO",
        "color": "purple",
    },
    "CONVERSATORIO": {
        "name": "CONVERSATORIO",
        "color": "green",
    }
}