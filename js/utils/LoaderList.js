class LoaderList{

    constructor(array, idElement){
        this.array = array
        this.element = document.getElementById(idElement);
        this.loadList();
    }

    loadList(){
        let article = {}, contentHTML = '';
        for(let i =0 ;i<this.array.length;i++){
            const {image, citation, ...information} = this.array[i];
            article = this.crearArticle(`article:${i}`);
            this.crearHeadHTML(article, image, `head:${i}`);
            contentHTML = this.createContentHTML(article, `content:${i}`);
            this.createCitation(contentHTML, citation);
            const data = { ...information };
            this.createInformation(contentHTML, data, citation);
            const { location } = citation;
            this.createButton(contentHTML, location);
        }
    }

    crearArticle(idElement){
       const element = `<article class="box-post lazy-show-visible" id="${idElement}" style="display:flex;"></article>`;
       this.element.innerHTML += element;

       return document.getElementById(idElement);
    }

    crearHeadHTML(element, image, id){
        const imageContent = `<div class="box-alpha" id="${id}">
                                 <div>
                                    <figure class="featured-image">
                                        <a href="${image}" target="_blank">
                                        <img src="${image}">
                                        </a>
                                    </figure>   
                                 </div>	 
                            </div>`;
        this.addContent(element, imageContent);
    }

    createContentHTML(element, id){
        const contentHTML = `<div class="box-beta" id="${id}" style="position:initial; width:100%; max-height:none;"></div>`;
        this.addContent(element, contentHTML);

        return document.getElementById(id);
    }

    createCitation(element, citation){
        const {location, presentationType, requiredTime} = citation;
        
        const {locationName} = locations[location]
        const {name, color} = presentationsType[presentationType]; 
        const citationHTML =  `<ul class="tags">
                                    <li class="tag-timestamp"><a href="video.html?id=${location}">${locationName}</a></li>
                                    
                                    <li class="tag-category" style="background-color:${color};">${name}</li>
                                    <li class="tag-category">${requiredTime}</li>
                                </ul>`;
        this.addContent(element, citationHTML); 
    }

    createInformation(element, information, citation){
        const {title, speakers, description} = information;
        const {location, presentationType, requiredTime} = citation;
        let speakersHTML = ``;
        for (let i = 0; i < speakers.length; i++) {
            speakersHTML += `<p > `+speakers[i].name+`  <a href="`+speakers[i].workContact+`" target="_blank">`+speakers[i].workContact+`</a> </p>`;
            
        }
        const informationHTML = `<div class="box-content" >
                                    <h2 class="box-title"><a href="video.html?id=`+citation.location+`">${title}</a></h2>
                                        ${speakersHTML}
                                    <p>
                                       ${description}
                                    </p>
                                    <p> &nbsp; </p>
                                </div>`;
        this.addContent(element, informationHTML); 
    }
    createButton(element, location){
        const {locationName} = locations[location];
        const button = `<div class="meta">
                            <a class="more-link" href="video.html?id=${location}"><span data-hover="VER AHORA">IR A: ${locationName}</span></a>
                        </div>`;
        this.addContent(element, button);
    }
    addContent(element, content){
        element.innerHTML += content;
    }
}
const loaderList = new LoaderList(presentations, 'presentations');