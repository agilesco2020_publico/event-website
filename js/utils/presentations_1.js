const presentations = [
  //agregar tipo depresentaciones
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/foto.png",
    citation: {
      location: "MUSEO_DEL_ORO", //para obtener la presentacion del archivo location.js
      presentationType: "PRESENTACION",
      requiredTime: "45 MIN",
    },
    title:
      'Product increment "potentially"​ releasable every sprint...IS NOT ENOUGH!',
    speakers: [
      {
        name: "Diego A. Gil",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/diegogil89/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "Scrum es claramente uno de los frameworks más populares usados en el mundo ágil, y por ende ha influenciado mucho la forma de entender y hacer agile. En el scrum guide, se habla de que Incremento de Producto potencialmente releaseable cada sprint, de hecho su busca 'potentially releasable' lo encuentres 6 veces. Pero ¿qué significa esto? ¿qué nos habilita? ¿qué nos limita? Para si algo he aprendido en agilidad es que las practicas sirven hasta que dejan de tener sentido. En esta sesión los invito a desafiarnos y a romper preceptos de scrum: Product increment \"potentially \" releasable every sprint is not enough!",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/image (1).png",
    citation: {
      location: "VALLE_DEL_COCORA",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Agile Marketing: el arte de ahorrar tiempo, minimizar recursos y mejorar resultados.",
    speakers: [
      {
        name: " Eliana Hernández",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/elianahernandezameson'>Linkedin</a>&nbsp",
      },
      {
        name: " Sebastián Muñoz",
        workContact:
          "<a target='_blank' href=' https://www.linkedin.com/in/sebastian-munoz-castaneda'>Linkedin</a>&nbsp",
      },
      {
        name: "Amalia Molina",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/amalia-molina-31a996a3/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "Cómo saber si estamos entregando valor real a los clientes? Cómo ir un paso adelante de la competencia? Cómo conocer de primera mano qué opina el usuario final de nuestros productos y servicios? Cómo tomar acciones tempranas que maximicen el valor entregado? Éstas son preguntas que normalmente nos hacemos y que durante nuestra sesión tendrás la oportunidad de aterrizar cómo de manera fácil y eficiente se pueden abordar &nbsp<br/> Link del Video: &nbsp<a target='_blank' href='https://drive.google.com/file/d/1S1mUYfePF3LC115nESrW8icAayvucSCY/view?usp=sharing'>Video</a>&nbspCompartiremos cómo hemos implementado Agile Marketing, cómo hemos superado las piedras en nuestro camino y nuestras victorias.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/portadaAgilesCo.JPG.jpg",
    citation: {
      location: "PARQUE_TAYRONA",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Business Agility en tiempos VUCA",
    speakers: [
      {
        name: "Luis Miguel Gonzalez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/luis-miguel-gonzalez-bernal/'>Linkedin</a>&nbsp",
      },
      {
        name: "Adriana Navas",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/adriana-navas-tarquino-50a57226/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      '"Usted no puede implementar agile, solo ayudarlo a crecer" Con lo anterior en mente charlaremos sobre como una telco ha venido descubriendo su camino en agilidad hasta encontrarse en un punto que lo podemos relacionar con business agility.',
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/Space_x_-_Boing.png",
    citation: {
      location: "RIO_PANCE", //para obtener la presentacion del archivo location.js
      presentationType: "PRESENTACION",
      requiredTime: "45 MIN",
    },
    title:
      "Space X: Llegando al espacio con agilidad / Boing: La deuda técnica puede hacer desaparecer tu empresa",
    speakers: [
      {
        name: "Daniel Ramírez",
        workContact:
          "<a target='_blank' href='https://agile.nassao.com/'>Blog</a>&nbsp  @nassao",
      },
    ],
    description:
      "Aplicando metodologías ágiles, Space X ha logrado convertirse en la empresa civil más reconocida en la carrera espacial.\
      Debido a la acumulación de deuda técnica asumida por la empresa, Boing está atravesando el peor momento de su historia.\
      Un paralelo entre ambas empresas que espero te pueda convencer que sí es posible adaptarse al cambio, sin importar el tipo de producto, el sector de la empresa, o incluso el presupuesto que requiera cualquier desarrollo.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/aco.png",
    citation: {
      location: "PARQUE_BERRIO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Certified Agile Testing",
    speakers: [
      {
        name: "Marcelo Lujan",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/lujanmarcelo'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "En 45 minutos veremos los principales aprendizajes de esta certificación otorgada por la iSQI, conocimiento y prácticas de testing en el desarrollo ágil.\
      Aprenderemos a ejecutar pruebas ágilmente y con excelentes resultados. Tomando en cuenta las teorías que nos proponen los principios del manifiesto ágil, entornos de trabajo, procesos y métodos.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/Agiles_Colombia_OKR.jpg",
    citation: {
      location: "PUNTA_GALLINAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "El camino para construir un OKR y mantenerlo en el tiempo",
    speakers: [
      {
        name: "Elio Said Roman Lamk",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/eliosaidromanlamk'>Linkedin</a>&nbsp",
      },
      {
        name: "Michel Martinez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/Michel--Martinez'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "Te contaremos las vivencias de ver nacer los OKR´s dentro de una organización TELCO y cómo hacerlos crecer y madurar en el tiempo, apalancado en el Business Agility",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/Diapositiva1.png",
    citation: {
      location: "CANO_CRISTALES",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Líder Consciente 4.0 para el mundo 4.0",
    speakers: [
      {
        name: "Rose Mery Restrepo Vélez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/roserestrepov/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "En un mundo 4.0 emergen líderes conscientes que crecen y evolucionan conjuntamente con las personas de su organización viviendo su propósito superior. Conoceremos los enfoques, tendencias para lograr la consciencia como punto clave para el crecimiento de las industrias 4.0",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/catalizador_de_equipos_acol.png",
    citation: {
      location: "DESIERTO_TATACOA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Conviértete en un CATALIZADOR DE EQUIPOS e impacta a tu cliente",
    speakers: [
      {
        name: "Rox Muñoz",
        workContact:
          "<a target='_blank' href='https:/roxmunoz.com/blog'>Blog</a>&nbsp @RoxMunoz_ en instagram y twitter",
      },
    ],
    description:
      "Una de las maneras más eficientes de enfrentarnos a este mundo VUCA es crear equipos de alto desempeño y que sean ellos quienes se adapten y satisfagan las necesidades del cliente. En este webinar aprenderás cómo convertirte en CATALIZADOR DE EQUIPOS, cuál es el mindset necesario y las herramientas que lo habilitan.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/Imagen1.png",
    citation: {
      location: "SANTUARIO_LAJAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Zoom y Mural, tips para sacar provecho de estas herramientas en esta época de cambio.",
    speakers: [
      {
        name: "Edwin Trujillo",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/edwin-trujillob/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "Zoom y Mural son las dos herramientas que mas uso en mis encuentros, quiero compartir con ustedes una serie de trucos y buenas prácticas para sacarle provecho.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/colombia2.png",
    citation: {
      location: "VILLA_DE_LEYVA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Creando tu Way of Working con Disciplined Agile",
    speakers: [
      {
        name: "Yoko Perez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/yoko-perez-a159687a/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "DA proporciona orientación para ayudar a las organizaciones a elegir su forma de trabajar (WoW) de una manera sensible al contexto, proporcionando una base sólida para la agilidad empresarial\
    DA es un conjunto de herramientas de estrategias y prácticas ágiles para complementar cualquier marco ágil como Scrum o SAFe",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/foro_errores.png",
    citation: {
      location: "CAÑON_CHICAMOCHA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN",
    },
    title: "Foro errores cometidos: c*gadas en agilidad y lecciones aprendidas",
    speakers: [
      {
        name: "Juan Andrés Ochoa",
        workContact: "<a href='https://www.linkedin.com/in/juanandres-ochoa/?originalSubdomain=co'>Linked</a>"
      },
      {
        name: "Miguel Maldonado",
        workContact: ""
      },
       {
        name: "Carlos Serna",
        workContact: ""
      }
      ,
      {
        name: "Jorge Abad",
        workContact: "<a href='https://www.linkedin.com/in/jorgeabadl/'>Linked</a>"
      }

      
     
    ],
    description: "Han sido unos años divertidos, el camino cuesta arriba, lleno de obstáculos pero también pletórico de oportunidades y desafíos apasionantes. Y los hemos enfrentado con ahínco, mirando hacia atrás solo para aprender de los incontables experimentos que hemos realizado en varios países, muchas empresas, cientos de equipos y con miles de personas. El aprendizaje nos ha traído hasta este momento, dispuestos apenas a continuar enriqueciendo el movimiento ágil. \
\
    Y queremos aprovechar esta gran oportunidad que nos brinda Ágiles Colombia 2020 para compartir con ustedes esas grandes embarradas que hemos cometido, que no son sino experimentos fallidos, pero de los que también hemos asimilado causas, extraído soluciones y hemos seguido adelante en este camino de la agilidad. En el fondo, no es tan poético como suena, son heridas de guerra, imborrables, pero están ahí para recordarnos que siempre es posible mejorar.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/imagen.png",
    citation: {
      location: "BAHIA_SOLANO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN",
    },
    title:
      "Que rayos es eso del Time to market,¿Puedo apoyar como Programador?",
    speakers: [
      {
        name: "Etna Estrella",
        
        workContact:                                                                                                                                                                
          "<a target='_blank' href='https://www.linkedin.com/in/etna-estrella-ec1'>Linkedin</a>&nbspBlog: <a target='_blank' href='https://www.etnaestrella.com'>Etna Estrella</a>&nbspTwitter: <a target='_blank' href='https://twitter.com/anthenoge/'>@anthenoge</a>",
      },
    ],
    description:
      "Mi nombre es Etna Estrella me formé y empecé siendo desarrolladora de software hace 14 años, he ido por este hermoso camino y luego por el de la arquitectura empresarial, el coaching, la innovación y continúo aprendiendo del arte. Deseo compartirles un poco de conocimiento bajo mi experiencia en esta ocasión e incrementarlo de manera colectiva, ¿Te atreves a experimentar?\
    En varias ocasiones se nos dice en los proyectos agiles que es necesarios reducir el “time to market”, ¿sabes qué hacer cuando te hablan de eso? También suelen decir que los marcos de trabajo agiles nos permiten reducir este famoso “time to market”, para las áreas gerenciales de una organización parece que es súper importante ¿Es importante para nosotros los programadores?, y ¿cómo programadores es posible aportar en esta reducción tan deseada del “time to market”?¿ lo tenemos claro? ¿sabemos que es?, nos hemos preguntado cómo podemos aportar? tengo unas cuantas ideas deseo generar conocimiento con ustedes mediante un ejercicio.\
    Te gusta el software te espero en esta sesión en la que aprenderemos que es el time to market y cómo podemos aportar desde una visión de desarrollo de software.",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/Sin_título.png",
    citation: {
      location: "LAGUNA_DE_TOTA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Agilidad organizacional: Equilibrio entre Frameworks e incertidumbre.",
    speakers: [
      {
        name: "Camilo Velásquez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/camilovelasquezardila/'>Linkedin</a>&nbsp",
      },
    ],
    description:
      "Muchas organizaciones deciden comenzar su evolución organizacional apoyándose en la agilidad, haciendo algún experimento con equipos Scrum, Kanban, etc.\
    Los buenos resultados a esta escala generan grandes expectativas para llevar la agilidad a toda la organización.\
    Llevar los éxitos de los principios ágiles a toda la organización representan un reto para el cual han ido surgiendo distintos movimientos y frameworks.\
    En esta charla hablaremos sobre los aspectos principales a tener en cuenta al momento de embarcarse en el camino de la agilidad organizacional, realizándonos preguntas del tipo ¿Debería usar un framework?, ¿Por donde debe comenzar?, ¿Cambios a pequeña o gran escala?    Acompáñame a compartir algunos aprendizajes en el camino de la agilidad organizacional!",
  },
  {
    initHour: "10:40 A.M",
    image: "content/images/sessions_1/jaime_julio.jpeg",
    citation: {
      location: "PARQUE_SAN_AGUSTIN",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "El Enfoque Empírico en Scrum",
    speakers: [
      {
        name: "Jaime Julio Avalos",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/jaimejulio/'>Linkedin</a>&nbsp",
      },
    ],
    description: "En esta oportunidad compartiré la importancia de ser coherente y consecuente con el empirismo en Scrum, qué significa trabajar en base a hechos, experiencia y evidencia, y cómo vivimos esto en los eventos de Scrum logrando el cambio cultural, agilidad empresarial, y mantener la Entrega de Valor.",
  },
  {
    initHour: "10:40 A.M",
    image:
      "content/images/sessions_1/De_la_procrastinación_a_la_Ultraproductividad.jpg",
    citation: {
      location: "PARQUE_NEVADOS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "De la Procrastinación a la Ultra productividad",
    speakers: [
      {
        name: "Fausto Roa",
        workContact: "",
      },
      {
        name: "Harold Rincón",
        workContact: "",
      },
    ],
    description:
      'En un mundo hiper conectado donde la información está a un click, la saturación de contenido a generado una distracción sin precedentes, hoy nos enfrentamos a un desafío mayor, que es saber priorizar toda esta información y herramientas para que nos ayuden a ser mas eficientes y productivos. Por eso te invitamos a nuestra charla, "De la procrastinación a la Ultra productividad".',
  },
];
