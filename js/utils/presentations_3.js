const presentations = [
  //agregar tipo depresentaciones

  {
    initHour: "12:30 P.M",
    image: "content/images/sessions_3/1.png  ",
    citation: {
      location: "MUSEO_DEL_ORO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Agilidad Aplicada... ¡¡¡ Conectando con Fundaciones !!!", 
    speakers: [
      {
        name: "Oscar LLanos Quiñones",
        workContact: "www.linkedin.com/in/memo-llanos",
      },
      {
        name: "Eder Fernando Pinzon",
        workContact: "",
      },
    ],
    description:
      "Desde hace más de un año venimos ayudando a diferentes fundaciones con base en prácticas ágiles aplicadas a lo largo del ciclo de vida de las mismas, aportanto en la concepción de sus productos o servicios, el ecosistema que los rodea, en la planeación estratégica y la definición de sus objetivos. Queremos mostrarles el trabajo realizado desde Activistas Constructivos* y alentarlos a sumarce a la causa... Unidos somos más y halamos más parejo..!!! \
<a target='_blank' href='https://activistasconstructivos.org/'> (*)https://activistasconstructivos.org/ </a> \
      ",
  },
  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/2.png",
    citation: {
      location: "VALLE_DEL_COCORA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Respuesta ante la vida real sobre seguir recetas ágiles",
    speakers: [
      {
        name: "Florencia Maneiro",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/florenciamaneiro/'>Linkedin</a>",
      },
    ],
    description:
      'Podes ser novato y ser ágil! \
      Esta es la historia, contada por una novata,  de cómo en la vida real, se puede ser ágil sin saber, ni siquiera, el significado de la palabra.\
      No puedo dar cátedra de muchos temas teóricos del mundo agile, no soy docente ni facilitadora profesional, ni mucho menos puedo realizar una sesión de coaching. Tampoco me acuerdo los principios de Lean ni todos los del manifiesto. \
      Pero si puedo, contar una historia de la agilidad en la vida real. Una historia de transformación y de cambio en la cual, unos novatos "en el mundo agile",  un día tuvieron coraje, se organizaron en grupo y se lanzaron al vacío siguiendo sus valores, principios e intuición. ',
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/3.png",
    citation: {
      location: "PARQUE_TAYRONA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Validando hipótesis de negocio",
    speakers: [
      {
        name: "Alex Canizales Castro.",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/alcaniza/'>Linkedin</a>&nbsp<a target='_blank' href='https://www.linkedin.com/in/alcaniza/'>Twitter</a>",
      },
    ],
    description:
      "Las hipótesis son supuestos sobre cómo esperamos que sea algo pero que en la práctica no tenemos suficientes datos para juzgar si es real, aplicando técnicas de <strong>innovación</strong> podemos reducir el  <strong>riesgo</strong> dentro del contexto organizacional.\
      Los resultados deben ser medidos para determinar los beneficios reales de las hipotesis frente a los planeados para decidir si <strong>pivotear</strong> o <strong>perseverar</strong>, en esta presentación quiero compartirles desde elegir el experimento correcto en el momento correcto y diseñar las secuencias de <strong>experimentos</strong>, varias formas de probar las ideas como alternativas a integrar en tu forma de trabajo, hasta la toma de <strong>datos</strong> que influyen en las decisiones para asi crear una cultura de experimentación, gestión de <strong>portafolios</strong> y financiación de iniciativas.\
      Esto requiere comodidad con la acción y renunciar a las prácticas tradicionales de analisis/diseño que a menudo crean desperdicios, extención el tiempo de ciclo y contribuye a la <strong>parálisis por análisis</strong>. Los participantes saldran entendiendo por qué y cómo la experimentación acelera el aprendizaje rápido y agilidad empresarial.",
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/4.jpg",
    citation: {
      location: "RIO_PANCE",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      " El Auténtico Scrum Master",
    speakers: [
      {
        name: "Giovanny Cifuentes",
        workContact:
          "<a target='_blank' href='http://giovannycifuentes.com/'>Blog</a>",
      },
    ],
    description:
      "El incomprendido rol de Scrum Master genera muchas confusiones en las organizaciones, muchas contratan sin conocer la importancia del rol y él porque es necesario para una adopción de prácticas agiles. El SM es el rol que siembra la agilidad y la impulsa de abajo hacia arriba donde sus enseñanzas son el cimiento de un cambio cultural en las organizaciones. \
      En la siguiente sesion de 45 minutos presento una propuesta de las características que considero debe tener un Autentico Scrum Master.",
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/paola_becerra.jpeg",
    citation: {
      location: "PARQUE_BERRIO",
      presentationType: "TALLER", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Ágil no es suficiente.... Una visión holistica",
    speakers: [
      {
        name: "Paola Becerra",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/paola-becerra-c%C3%A1rdenas-b4539975/'>Linkedin</a>",
      },
    ],
    description:
      "ÁGIL NO ES SUFICIENTE...\
      Una visión integral de la agilidad para el logro de resultados organizacionales.",
  },
  {
    initHour: "12:30 A.M",
    image: "content/images/sessions_3/samuel.jpeg",
    citation: {
      location: "PUNTA_GALLINAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "LECCIONES APRENDIDAS: CALIDAD DE SOFTWARE ADAPTATIVO ¡ROMPIENDO PARADIGMAS ORGANIZACIONALES!",
    speakers: [
      {
        name: "Samuel Castillo Rincon",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/hebert-samuel-castillo-rincon-34128b179/'>Linkedin</a>",
      },
    ],
    description:
      "En este espacio aprenderemos algunas de las lecciones aprendidas durante un proceso de QA que se adapta al agilismo en los clientes, organización.  Del mismo modo como nosotros mismos podemos ir rompiendo los paradigmas que existen en muchas organizaciones, y así lograr que exista un plus cuando estamos realizando un buen Agile Testing, y así agregar valor en el desarrollo de software y en su calidad."
     },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/5.png",
    citation: {
      location: "CANO_CRISTALES",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Lancémonos a experimentar",
    speakers: [
      {
        name: "Alejandra Reyes Reina",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/alejandra-reyes-reina-b3486124/'>Linkedin</a>",
      },
      {
        name: "Oscar Mauricio Ballesteros",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/oscar-mauricio-ballesteros-bb476325/'>Linkedin</a>",
      },
    ],
    description:
      "De donde nacen las hipótesis?\
      Digital Marketing + Agile + Design Thinking",
  },
  {
    initHour: "12:30 A.M",
    image: "content/images/sessions_3/12.jpg",
    citation: {
      location: "DESIERTO_TATACOA",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN DESDE LAS 12:30 A.M",
    },
    title: "El camino de la Transformación no es como lo pintan",
    speakers: [
      {
        name: "Jovan Gil",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/jovangil/'>Linkedin</a>",
      },
      {
        name: "Herlency Muñoz",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/herlency/'>Linkedin</a>",
      }
    ],
    description:
      "¿Cómo empezar un proceso de Transformación?, ¿Qué es lo primero que necesitamos transformar?, y ¿cuándo finaliza un proceso de Transformación?. Son este tipo de “preguntas que no dejan dormir” las que nos planteamos todos los días cuando pensamos en Transformación.\
      El camino no nos brinda todas las soluciones. Se necesita aprender, desaprender y volver a aprender para darle forma a esa ruta transformacional de la que tanto se habla.\
      En este espacio queremos contarles el camino que ha transitado Ceiba para obtener nuestras propias bases de Transformación adaptando este proceso a nuestra realidad y nuestras necesidades.\
      El camino no es fácil, no es como lo pintan, y es diferente para todos.",
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/6.jpeg",
    citation: {
      location: "SANTUARIO_LAJAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Liderazgo en la Era del Conocimiento",
    speakers: [
      {
        name: "Cristian Bermudez",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/cristian-bermudez/'>Linkedin</a>",
      },

    ],
    description:
    "En la actualidad nos enfrentamos a retos intrincados y complejos. La transformación digital lleva a que las cosas en nuestro mundo pasen muy rápido, un individuo simplemente no puede resolver todo por sí mismo. El liderazgo se adaptó a estos entornos, te invito a que hablemos sobre las características del líder servicial en esta \"Era del Conocimiento\".",
  },

  {
    initHour: "12:30 A.M",
    image: "content/images/sessions_3/aura_villagrana.jpg",
    citation: {
      location: "VILLA_DE_LEYVA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "High Performance Scrum Master",
    speakers: [
      {
        name: "Aura Villagrana",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/auravillagrana/'>Linkedin</a>",
      },
    ],
    description:
      "Un Scrum Master certificado no implica el éxito en el desarrollo de productos de valor, ¿Qué es lo que debe hacer muy bien el Scrum Master?, ¿Qué significa dedicarse al coaching y a la educación del equipo para la adopción de Scrum? ¿Cómo se genera la pasión para transformar la forma en como se colabora para la creación del valor: Equipo y Organización? Esta plática es para los Scrum Masters que tienen que enfocarse en lo esencial de su rol",
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/8.png",
    citation: {
      location: "CAÑON_CHICAMOCHA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Agile Yin + UX Yang  (En Inglés)",
    speakers: [
      {
        name: "Toby V Rao",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/tobyrao/'>Linkedin</a>",
      },
      {
        name: "Jim Williams",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/jim-williams-ab73662/'>Linkedin</a>",
      },
      {
        name: "Carlos Arturo Quiroga",
        workContact: "<a target='_blank' href='https://linkedin.com/in/carlosarturoq/'>Linkedin</a>",
      },

    ],
    description:
    "On the surface, Agile and UX appear so different…but in reality, they have a whole lot in common. The philosophies are complementary and highly supportive of each other.\
    All that sounds fab… but blending UX and Agile in the real world and making it work is super tough.\
    But wait! Don’t lose heart! It can be done! Please join us in this session to learn from Jim Williams and Toby Rao! They have some great tricks and tips up their sleeves!\
    Alright, so the implementation problems may happen but those can be resolved. Starting small, going steady, and scaling it in a nice and easy pace is the key. Integrating UCD and Agile methodologies take the right skills, a good plan, and the right people.\
    Toby and Jim will share great ideas, tips, and some proven methods to apply a balanced integration to deliver customer nirvana. The duo will also share some real-life stories on how their teams deliver exactly what their customers need by using the magic of Agile Yin and UX Yang!",
  },

  {
    initHour: "12:30PM",
    image: "content/images/sessions_3/9.png",
    citation: {
      location: "BAHIA_SOLANO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: " Mindset Ágil el Monstruo de las implementaciones",
    speakers: [
      {
        name: "Sandra Arias Andrade",
        workContact: "<a target='_blank' href='https://www.linkedin.com/in/sandraariasandrade/'>Linkedin</a>",
      },
    ],
    description:
    "Muchas transformaciones se han detenido porque no logramos cambiarle el chip a las personas, por que el Mindset Agil no esta incorporado en los lideres y las personas de la organización. El mindset se ha convertido en la excusa perfecta, el mostruo perfecta que nos aleja de enfocar los esfuerzos en los resultados de transformación!.",
  },

  
  {
    initHour: "12:30 P.M",
    image: "content/images/sessions_3/lucho_salazar.jpeg",
    citation: {
      location: "PARQUE_NEVADOS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "CÓMO LOS DUEÑOS DE PRODUCTO VIRTUOSOS PUEDEN Gestionar Mejor las Historias de Usuario",
    speakers: [
      {
        name: "Lucho Salazar",
        workContact:
        "<a target='_blank' href='https://www.linkedin.com/in/luchosalazar/?originalSubdomain=co'>Linkedin</a>",
 
      },
    ],
    description: "Algunas recomendaciones prácticas para que los Dueños de Producto empiecen a realizar un mejor tratamiento de las historias de usuario como elementos de primera ciudadanía en el backlog de producto. Si no eres Dueño de Producto, estas sugerencias te pueden servir para acompañarlo mejor en el proceso de adopción de nuevos comportamientos útiles y apropiados para la cultura ágil que necesitamos hoy en las organizaciones.",
  }

  
];
