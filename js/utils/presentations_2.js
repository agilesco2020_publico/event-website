const presentations = [
  //agregar tipo depresentaciones

  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/1.jpg",
    citation: {
      location: "MUSEO_DEL_ORO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "ADKAR, logrando un cambio efectivo en las organizaciones",
    speakers: [
      {
        name: "Carlos Palacio",
        workContact:
          "<a target='_blank' href='www.linkedin.com/in/palaciocarlos'>Linkedin</a>",
      },
    ],
    description:
      "Veremos a través del modelo ADKAR, desarrollado por PROSCI, cómo podemos navegar por los procesos de cambio organizacionales más efectivamente si nos enfocamos en las personas. Conversaremos de cada uno de los elementos del modelo Awareness (Consciencia), Desire (Deseo), Knowledge (Conocimiento), Ability (Habilidad), Reinforcement (Refuerzo)",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/julian_mendoza.jpg",
    citation: {
      location: "PARQUE_TAYRONA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "La Gestión de Riesgos en Entornos Agile",
    speakers: [
      {
        name: "Julián Mendoza",
        workContact: "<a target='_blank' href='http://linkedin.com/in/jmendozat41'>Linkedin</a>",
      },
    ],
    description: "Descripción de la propuesta: Pasar de marcos tradicionales donde los proyectos son gestionados a través de métodos como Monte Carlo y simulaciones exhaustivas que permitan, reducir, mitigar y transferir el riesgo de los proyectos al uso de marcos ágiles. Surgen varios interrogantes en los patrocinadores, directivos y líderes sobre: ¿cómo gestionamos el riesgo de los proyectos? y ¿Qué herramientas o elementos usamos en relación con el riesgo? \
    En este espacio te contaré como el Riesgo sigue siendo clave y como funciona y se gestiona dentro de los proyectos bajo Agile.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/2.jpg",
    citation: {
      location: "VALLE_DEL_COCORA", //para obtener la presentacion del archivo location.js
      presentationType: "TALLER",
      requiredTime: "45 MIN",
    },
    title: "¿Queremos cambiar?",
    speakers: [
      {
        name: "Diego Virgolini",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/dvirgolini/'>Linkedin</a>&nbspTwitter: <a target='_blank' href='https://twitter.com/dvirgolini/'>@dvirgolini</a>",
      },
    ],

    description:
      "Un espacio para reflexionar sobre qué, cómo y para qué cambiar.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/6.png",
    citation: {
      location: "RIO_PANCE",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Estimar o NoEstimate Esa es la cuestión.",
    speakers: [
      {
        name: "Erik Sarmiento",
        workContact: "",
      },
    ],
    description:
      "En este espacio busco poder contarles que es eso de lo que se está hablando en muchos lados #NoEstimate, también mostrarles como lo pude aplicar en uno de mis equipos y como estoy realizando la transición en los otros, todo va a girar entorno a lo que he vivido y que beneficios he obtenido.",
     },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/3.jpg",
    citation: {
      location: "PARQUE_BERRIO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "60 MIN",
    },
    title: "unBOXING Patrones de Scrum",
    speakers: [
      {
        name: "Anyi Ximena Barrero",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/anyi-barrero-037899167/'>Linkedin</a> Link Video: <a target='_blank' href='https://www.dropbox.com/s/sjlpeij85q5y3cu/AgilesCoScrum.mp4?dl=0'>Presentación</a>",
      },
      {
        name: "Robinson Rico",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/robinson-rico-mendez-14511666/'>Linkedin</a>",
      },
      {
        name: "Rodrigo Caro Sánchez",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/rodrigo-andres-caro-sanchez-6807498a/ '>Linkedin</a>",
      },
    ],

    description:
      "Se dará una introducción a lo que son y que proponen los patrones de Scrum; los cuales recogen la experiencia de la comunidad respecto a que se recomienda hacer cuando tenemos una situación problemática al implementar Scrum. La presentación se orienta a los patrones a tener en cuenta para la configuración de un producto/proyecto en su inicio y refuerza aspectos relevantes que olvidamos en el día a día de los patrones core (353).",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/7.png",
    citation: {
      location: "PUNTA_GALLINAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Experiencia reales aplicando Management 3.0 al cambio organizacional",
    speakers: [
      {
        name: "Ezequiel Kahan ",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/ekahan/'>Linkedin</a>&nbspTwitter: <a target='_blank' href='https://twitter.com/soyezequiel/'>@soyezequiel</a>&nbspKnowment: <a target='_blank' href='https://knowment.net/ezequiel-kahan/'>Blog</a>",
      },
    ],
    description:
      "En esta presentación vamos a ver como Management 3.0, además de brindarnos un conjunto sólido de herramientas, nos provee del soporte para acompañar u proceso de transformación / evolución organizacional sostenible. Para eso, voy a compartir una experiencia concreta de transformación organizacional utilizando mindset, técnica y herramientas de Management 3.0. ",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/8.png",
    citation: {
      location: "CANO_CRISTALES", //para obtener la presentacion del archivo location.js
      presentationType: "PRESENTACION",
      requiredTime: "45 MIN",
    },
    title: "Creando equipos todoterreno",
    speakers: [
      {
        name: "John Gutierrez ",
        workContact: 
          "<a target='_blank' href='https://medium.com/@oichi0620'>Medium</a>&nbspTwitter: <a target='_blank' href='https://twitter.com/oichi620/'>@oichi620</a>",
      },
    ],
    description:
      "¿Estas encantado de la agilidad, pero tu equipo de trabajo no?</p>\
      ¿Sufre de egos dentro de su equipo de trabajo?</p>\
      ¿Al finalizar un iteración, tu equipo de trabajo odian entre todos?</p>\
      ¿No ahí tiempo para experimentar, por que tenemos que salir a producción?</p>\
      ¿Ya se dio por vencido por ese equipo que siempre falla?</p>\
      Si respondiste a más de una de estas preguntas, ¡si señor!. Te invito a esta pequeña charla donde te contare experimentos para probar con tus equipos el mismo lunes, me han funcionado a mi, no es la biblia pero seguro que algo te puede ayudar",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/9.jpg",
    citation: {
      location: "DESIERTO_TATACOA",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN",
    },
    title: "El camino de la Transformación no es como lo pintan",
    speakers: [
      {
        name: "Jovan Gil",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/jovangil/'>Linkedin</a>",
      },
      {
        name: "Herlency Muñoz",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/herlency/'>Linkedin</a>",
      },
    ],
    description:
      "¿Cómo empezar un proceso de Transformación?, ¿Qué es lo primero que necesitamos transformar?, y ¿cuándo finaliza un proceso de Transformación?. Son este tipo de “preguntas que no dejan dormir” las que nos planteamos todos los días cuando pensamos en Transformación.\
      El camino no nos brinda todas las soluciones. Se necesita aprender, desaprender y volver a aprender para darle forma a esa ruta transformacional de la que tanto se habla.\
      En este espacio queremos contarles el camino que ha transitado Ceiba para obtener nuestras propias bases de Transformación adaptando este proceso a nuestra realidad y nuestras necesidades.\
      El camino no es fácil, no es como lo pintan, y es diferente para todos.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/8.jpg",
    citation: {
      location: "SANTUARIO_LAJAS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Liderazgo y equipos en la virtualidad.",
    speakers: [
      {
        name: "Andrés Felipe Triana Gallego.",
        workContact:
          "<a target='_blank' href='https://www.linkedin.com/in/andrestriana3312/'>Linkedin</a>",
      },
    ],
    description:
      "En la actualidad, se presenta un contexto de crisis a causa de la COVID-19, lo que obliga a las personas y a las organizaciones, a desarrollar las capacidades que les permitan adaptarse al cambio al que se ven enfrentados, y así poder hacer frente a los retos que se les presentan. Entonces, ¿cómo los equipos pueden adaptarse a este cambio?.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/10.jpg",
    citation: {
      location: "VILLA_DE_LEYVA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Gestión del Cambio & Agile",
    speakers: [
      {
        name: "Iván Martinez",
        workContact: "",
      },
    ],
    description:
      "Aprendamos juntos sobre las mejores prácticas basadas en investigación para liderar el lado humano del cambio y aumentar las probabilidades de éxito de los procesos de transición.\
      Accede al estudio de mejores prácticas de Gestión del cambio & Agil y a la serie del modelo ADKAR® de Prosci®.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_1/juan_ochoa.jpg",
    citation: {
      location: "CAÑON_CHICAMOCHA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN",
    },
    title: "Foro sobre errores cometidos",
    speakers: [
      {
        name: "Juan Ochoa",
        workContact: "",
      },
    ],
    description: " ",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/9.png",
    citation: {
      location: "BAHIA_SOLANO",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "100 MIN desde las 11:35 A.M",
    },
    title:
      "Que rayos es eso del Time to market,¿Puedo apoyar como Programador?",
    speakers: [
      {
        name: "Etna Estrella",
        workContact:                                                                                                                                                                
          "<a target='_blank' href='https://www.linkedin.com/in/etna-estrella-ec1'>Linkedin</a>&nbspBlog: <a target='_blank' href='https://www.etnaestrella.com'>Etna Estrella</a>&nbspTwitter: <a target='_blank' href='https://twitter.com/anthenoge/'>@anthenoge</a>",
      },
    ],
    description:
      "Mi nombre es Etna Estrella me formé y empecé siendo desarrolladora de software hace 14 años, he ido por este hermoso camino y luego por el de la arquitectura empresarial, el coaching, la innovación y continúo aprendiendo del arte. Deseo compartirles un poco de conocimiento bajo mi experiencia en esta ocasión e incrementarlo de manera colectiva, ¿Te atreves a experimentar?\
      En varias ocasiones se nos dice en los proyectos agiles que es necesarios reducir el “time to market”, ¿sabes qué hacer cuando te hablan de eso? También suelen decir que los marcos de trabajo agiles nos permiten reducir este famoso “time to market”, para las áreas gerenciales de una organización parece que es súper importante ¿Es importante para nosotros los programadores?, y ¿cómo programadores es posible aportar en esta reducción tan deseada del “time to market”?¿ lo tenemos claro? ¿sabemos que es?, nos hemos preguntado cómo podemos aportar? tengo unas cuantas ideas deseo generar conocimiento con ustedes mediante un ejercicio.\
     Te gusta el software te espero en esta sesión en la que aprenderemos que es el time to market y cómo podemos aportar desde una visión de desarrollo de software.\
     ¡Los espero con muchas ganas de participar y salir de nuestra zona de confort!!",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/11.jpg",
    citation: {
      location: "LAGUNA_DE_TOTA",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "Abriendo mentes en tiempo de pandemia",
    speakers: [
      {
        name: "Jorge Andrés Ramirez / Agile Coach y Agile Business Owner",
        workContact:
          "<a target='_blank' href='http://www.linkedin.com/in/jorgearamirezb'>Linkedin</a>",
      },
    ],
    description:
      "En marzo del 2020 la humanidad tuvo que dar un giro de 180 ° cambiando sus hábitos, rutinas y formas de actuar ante situaciones cotidianas, lo sencillo se volvió complejo y aquello que creíamos imposible de realizar se volvió posible. En esta sesión abordaremos como este valor del manifiesto “Respuesta al cambio sobre seguir un plan” nos da las bases necesarias de adaptación en tiempos cambiantes y turbulentos, conoceremos a través de casos prácticos el gran potencial de esta práctica en la actualidad, como todos podemos realizar un cambio de cultura, un mindset que nos permita potencializar nuestras habilidades para adaptarnos a cualquier entorno.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/10.png",
    citation: {
      location: "PARQUE_SAN_AGUSTIN",
      presentationType: "CONVERSATORIO", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title: "¿Cómo afrontar los cambios? Mentalidad ágil",
    speakers: [
      {
        name: "Karen Andrea Sánchez / Facilitador Ágil",
        workContact:
          "<a target='_blank' href='http://linkedin.com/in/karen-andrea-sanchez-bedoya-140558125'>Linkedin</a>",
      },
    ],
    description:
      "Este espacio está enfocado a dar tips de experiencias vividas para que los equipos ágiles estén preparados para afrontar los diferentes cambios que se puedan presentar. Está enfocado a líderes, scrum master, Product owner.",
  },
  {
    initHour: "11:35 A.M",
    image: "content/images/sessions_2/luis_enrique.jpg",
    citation: {
      location: "PARQUE_NEVADOS",
      presentationType: "PRESENTACION", //para obtener la presentacion del archivo presentationType.js
      requiredTime: "45 MIN",
    },
    title:
      "Scrum - Covid19 : La respuesta al Cambio, ante un plan en cuarentena.",
    speakers: [
      {
        name: "Luis Enrique Castro",
        workContact:
        "<a target='_blank' href='https://www.linkedin.com/in/luisenriquecastromaldonado/'>Linkedin</a>",
 
      },
    ],
    description: "En este espacio Te contaremos como una compañía de RETAIL - Financiero  logró  enfocar los esfuerzos de sus equipos ágiles en la construcción de productos digitales, para responder al cambio ante las nuevas necesidades del cliente en el confinamiento por la cuarentena, y apoyar su estrategia de supervivencia sobre el plan estratégico trazado desde principio de año.",
  },
];
