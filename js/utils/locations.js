const locations = { //archivo donde se agregan todas locaciones 
    "MUSEO_DEL_ORO": {
        "locationName": "MUSEO DEL ORO",
        "twitch_channel": "agilesco2020_1"
        
    },
    "PARQUE_TAYRONA": {
        "locationName": "PARQUE TAYRONA",
        "twitch_channel": "agilesco2020_2"
    },
    "VALLE_DEL_COCORA": {
        "locationName": "VALLE DEL COCORA",
        "twitch_channel": "agilesco2020_sala_3"
    }
    ,
    "RIO_PANCE": {
        "locationName": "RÍO PANCE",
        "twitch_channel": "agilesco2020_sala_4"
    }
    ,
    "PARQUE_BERRIO": {
        "locationName": "PARQUE BERRIO",
        "twitch_channel": "agilesco2020_sala_5"
    }
    ,
    "PUNTA_GALLINAS": {
        "locationName": "PUNTA GALLINAS",
        "twitch_channel": "agilesco2020_sala_6"
    }
    ,
    "CANO_CRISTALES": {
        "locationName": "CAÑO CRISTALES",
        "twitch_channel": "agilesco2020_sala_7"
    }
    ,
    "DESIERTO_TATACOA": {
        "locationName": "DESIERTO DE LA TATACOA",
        "twitch_channel": "agilesco2020_sala_8"
    }
    ,
    "SANTUARIO_LAJAS": {
        "locationName": "SANTUARIO DE LAS LAJAS",
        "twitch_channel": "agilesco2020_sala_9"
    }
    ,
    "VILLA_DE_LEYVA": {
        "locationName": "PLAZA VILLA DE LEYVA",
        "twitch_channel": "agilesco2020_sala_10"
    }
    ,
    "CAÑON_CHICAMOCHA": {
        "locationName": "CAÑON DEL CHICAMOCHA",
        "twitch_channel": "agilesco2020_sala_11"
    }
    ,
    "BAHIA_SOLANO": {
        "locationName": "BAHIA SOLANO",
        "twitch_channel": "agilesco2020_sala_12"
    }
    ,
    "LAGUNA_DE_TOTA": {
        "locationName": "LAGUNA DE TOTA",
        "twitch_channel": "agilesco2020_sala_13"
    }
    ,
    "PARQUE_SAN_AGUSTIN": {
        "locationName": "PARQUE SAN AGUSTIN",
        "twitch_channel": "agilesco2020_sala_14"
    }

    ,
    "PARQUE_NEVADOS": {
        "locationName": "PARQUE NATURAL DE LOS NEVADOS",
        "twitch_channel": "agilesco2020_sala_15"
    }
}




